// Copyright 2023 Dexter.Wan. All Rights Reserved. 
// EMail: 45141961@qq.com

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DTModelGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class DTMODEL_API ADTModelGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
